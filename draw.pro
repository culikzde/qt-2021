QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

HEADERS += \
    colorbutton.h \
    toolbutton.h \
    panel.h \
    draw.h

SOURCES += \
    colorbutton.cc \
    toolbutton.cc \
    panel.cc \
    draw.cc

FORMS += \
    draw.ui

RESOURCES += \
    resources.qrc

DISTFILES += \
    data/draw.html \
    data/easyui.html \
    data/table.json \
    data/tree.json \
    scripts/call.py \
    scripts/listen.py \
    scripts/qcall.py \
    scripts/qlisten.py \
    scripts/web.py

CONFIG += SQL
CONFIG += DBUS
CONFIG += JS
CONFIG += SVG_PANEL
CONFIG += SOCKET
CONFIG += WEBENGINE
CONFIG += WEBKIT
CONFIG += CHART
CONFIG += VISUALIZATION
CONFIG += QT3D

win32 {
   CONFIG -= WEBKIT
   gcc {
      CONFIG -= WEBENGINE
   }
   SOCKET {
      CONFIG -= VISUALIZATION
   }
}

greaterThan(QT_MAJOR_VERSION, 6) {
   CONFIG -= JS
   CONFIG -= WEBENGINE
   CONFIG -= WEBKIT
   CONFIG -= CHART
   CONFIG -= VISUALIZATION
}

SQL {
    QT += sql
    DEFINES += SQL
}

DBUS {
    QT += dbus
    DEFINES += DBUS
}

JS {
    QT += script scripttools
    DEFINES += JS
    HEADERS += js.h
    SOURCES += js.cc
    # dnf install qt5-qtscript-devel
}

SVG_PANEL {
    QT += svg network
    greaterThan(QT_MAJOR_VERSION, 5): QT += svgwidgets
    DEFINES += SVG_PANEL
    # dnf install qt5-qtsvg-devel
}

SOCKET {
    QT += network
    DEFINES += SOCKET
}

WEBENGINE {
    QT += webengine webenginewidgets
    DEFINES += WEBENGINE
    # dnf install qt5-qtwebengine-devel
}

WEBKIT {
    QT += webkit webkitwidgets
    DEFINES += WEBKIT
    # dnf install qt5-qtwebkit-devel
}

CHART {
    QT += charts
    DEFINES += CHART
    # dnf install qt5-qtcharts-devel
}

VISUALIZATION {
    QT += datavisualization
    DEFINES += VISUALIZATION
    # dnf install qt5-qtdatavis3d-devel
}

QT3D {
    QT += 3dcore 3dextras 3drender 3dinput
    DEFINES += QT3D
    # dnf install qt5-qt3d-devel
}

gcc {
   QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-unused-variable -Wno-parentheses
}

msvc {
   QMAKE_CXXFLAGS_WARN_ON -= -w34100 -w34189
   QMAKE_CXXFLAGS_WARN_OFF += -wd4100 -wd4189
}

unix {
# https://stackoverflow.com/questions/19066593/copy-a-file-to-build-directory-after-compiling-project-with-qt
copydata.commands = $(COPY_DIR) $$PWD/data/{draw.html,easyui.html,tree.json,table.json} $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
}
