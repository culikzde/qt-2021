
/* js.cc */

#include "js.h"

#include <QSyntaxHighlighter>
#include <QKeyEvent>
#include <QAbstractItemView>
#include <QSyntaxHighlighter>
#include <QScrollBar>

#define DEBUGGER

#include <QMimeData>
#include <QScriptEngine>
#include <QScriptValue>
#include <QScriptValueIterator>

#ifdef DEBUGGER
   #include <QMainWindow>
   #include <QScriptEngineDebugger>
#endif

#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

class JsHighlighter : public QSyntaxHighlighter
{
   // Q_OBJECT

   public:
      JsHighlighter (QTextDocument *parent = 0);

   protected:
      void highlightBlock (const QString & text) override;
      void highlightBlock2 (const QString & text);

   private:
      struct HighlightingRule
      {
         QRegExp pattern;
         QTextCharFormat format;
      };
      QVector<HighlightingRule> highlightingRules;

      QRegExp commentStartExpression;
      QRegExp commentEndExpression;

      QTextCharFormat keywordFormat;
      QTextCharFormat classFormat;
      QTextCharFormat singleLineCommentFormat;
      QTextCharFormat multiLineCommentFormat;
      QTextCharFormat quotationFormat;
      QTextCharFormat functionFormat;
};

JsHighlighter::JsHighlighter (QTextDocument *parent)
      : QSyntaxHighlighter (parent)
{
   HighlightingRule rule;

   keywordFormat.setForeground (Qt::blue);
   keywordFormat.setFontWeight (QFont::Bold);

   QStringList keywordPatterns;
   keywordPatterns << "char" << "class" << "const"
   << "double" << "enum" << "explicit"
   << "friend" << "inline" << "int"
   << "long" << "namespace" << "operator"
   << "private" << "protected" << "public"
   << "short" << "signals" << "signed"
   << "slots" << "static" << "struct"
   << "template" << "typedef" << "typename"
   << "union" << "unsigned" << "virtual"
   << "void" << "volatile"
   << "function" << "for";

   foreach (const QString &pattern, keywordPatterns)
   {
      rule.pattern = QRegExp ("\\b" + pattern + "\\b");
      rule.format = keywordFormat;
      highlightingRules.append (rule);
   }

   classFormat.setFontWeight (QFont::Bold);
   classFormat.setForeground (QColor ("darkorange"));
   rule.pattern = QRegExp ("\\bQ[A-Za-z]+\\b");
   rule.format = classFormat;
   highlightingRules.append (rule);

   singleLineCommentFormat.setForeground (Qt::red);
   rule.pattern = QRegExp ("//[^\n]*");
   rule.format = singleLineCommentFormat;
   highlightingRules.append (rule);

   multiLineCommentFormat.setForeground (Qt::red);

   quotationFormat.setForeground (Qt::darkGreen);
   rule.pattern = QRegExp ("\".*\"");
   rule.format = quotationFormat;
   highlightingRules.append (rule);

   functionFormat.setFontItalic (true);
   functionFormat.setForeground (QColor ("coral"));
   rule.pattern = QRegExp ("\\b[A-Za-z0-9_]+(?=\\()");
   rule.format = functionFormat;
   highlightingRules.append (rule);

   commentStartExpression = QRegExp ("/\\*");
   commentEndExpression = QRegExp ("\\*/");
}

void JsHighlighter::highlightBlock (const QString &text)
{
   foreach (const HighlightingRule &rule, highlightingRules)
   {
      QRegExp expression (rule.pattern);
      int index = expression.indexIn (text);
      while (index >= 0)
      {
         int length = expression.matchedLength ();
         setFormat (index, length, rule.format);
         index = expression.indexIn (text, index + length);
      }
   }
   setCurrentBlockState (0);

   int startIndex = 0;
   if (previousBlockState () != 1)
      startIndex = commentStartExpression.indexIn (text);

   while (startIndex >= 0)
   {
      int endIndex = commentEndExpression.indexIn (text, startIndex);
      int commentLength;
      if (endIndex == -1)
      {
         setCurrentBlockState (1);
         commentLength = text.length () - startIndex;
      }
      else
      {
         commentLength = endIndex - startIndex
                         + commentEndExpression.matchedLength ();
      }
      setFormat (startIndex, commentLength, multiLineCommentFormat);
      startIndex = commentStartExpression.indexIn (text, startIndex + commentLength);
   }
}

/* ---------------------------------------------------------------------- */

#ifdef COMPLETION

void JsEdit::setCompleter (QCompleter * c)
{
   if (completer)
      QObject::disconnect (completer, 0, this, 0);

   completer = c;

   if (!completer)
      return;

   completer->setWidget (this);
   completer->setCompletionMode (QCompleter::PopupCompletion);
   completer->setCaseSensitivity (Qt::CaseInsensitive);
   QObject::connect (completer, SIGNAL (activated (const QString&)),
                     this, SLOT (insertCompletion (const QString&)));
}

void JsEdit::setCompletion (QStringList list)
{
    QCompleter * completer = new QCompleter (list, this);
    completer->setModelSorting (QCompleter::CaseInsensitivelySortedModel);
    completer->setCaseSensitivity (Qt::CaseInsensitive);
    completer->setWrapAround (false);
    this->setCompleter (completer);
}

void JsEdit::insertCompletion (const QString & completion)
{
   if (completer->widget () != this)
      return;
   QTextCursor tc = textCursor ();
   int extra = completion.length () - completer->completionPrefix ().length ();
   tc.movePosition (QTextCursor::Left);
   tc.movePosition (QTextCursor::EndOfWord);
   tc.insertText (completion.right (extra));
   setTextCursor (tc);
}

QString JsEdit::textUnderCursor () const
{
   QTextCursor tc = textCursor ();
   tc.select (QTextCursor::WordUnderCursor);
   return tc.selectedText ();
}

void JsEdit::focusInEvent (QFocusEvent *e)
{
   if (completer)
      completer->setWidget (this);
   QTextEdit::focusInEvent (e);
}

void JsEdit::keyPressEvent (QKeyEvent *e)
{
   if (completer && completer->popup ()->isVisible ())
   {
      // The following keys are forwarded by the completer to the widget
      switch (e->key ())
      {
         case Qt::Key_Enter:
         case Qt::Key_Return:
         case Qt::Key_Escape:
         case Qt::Key_Tab:
         case Qt::Key_Backtab:
            e->ignore ();
            return; // let the completer do default behavior
         default:
            break;
      }
   }

   bool isShortcut = ( (e->modifiers () & Qt::ControlModifier) && e->key () == Qt::Key_Space); // CTRL+Space
   if (!completer || !isShortcut) // dont process the shortcut when we have a completer
      QTextEdit::keyPressEvent (e);

   const bool ctrlOrShift = e->modifiers () & (Qt::ControlModifier | Qt::ShiftModifier);
   if (!completer || (ctrlOrShift && e->text ().isEmpty ()))
      return;

   static QString eow ("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="); // end of word
   bool hasModifier = (e->modifiers () != Qt::NoModifier) && !ctrlOrShift;
   QString completionPrefix = textUnderCursor ();

   if (!isShortcut && (hasModifier || e->text ().isEmpty () || completionPrefix.length () < 3
                       || eow.contains (e->text ().right (1))))
   {
      completer->popup ()->hide ();
      return;
   }

   if (completionPrefix != completer->completionPrefix ())
   {
      completer->setCompletionPrefix (completionPrefix);
      completer->popup ()->setCurrentIndex (completer->completionModel ()->index (0, 0));
   }
   QRect cr = cursorRect ();
   cr.setWidth (completer->popup ()->sizeHintForColumn (0)
                + completer->popup ()->verticalScrollBar ()->sizeHint ().width ());
   completer->complete (cr); // popup it up!
}

void JsEdit::addToCompletion (QScriptValue branch)
{
   QStringList list;
   QScriptValueIterator iter (branch);
   while (iter.hasNext ())
   {
      iter.next ();
      list.append (iter.name ());
      info->append ("COMPLETION " + iter.name ());
   }
   setCompletion (list);
}

#endif

/* ---------------------------------------------------------------------- */

bool JsEdit::canInsertFromMimeData (const QMimeData * data) const
{
   return data->hasColor();
}

void JsEdit::insertFromMimeData(const QMimeData * data)
{
    QColor color = data->colorData().value <QColor> ();
    setTextColor (color);
    insertPlainText ("drop");
}

/* ---------------------------------------------------------------------- */

void JsEdit::setTree (QTreeWidget * p_tree)
{
   tree = p_tree;
}

void JsEdit::setInfo (QTextEdit * p_info)
{
   info = p_info;
}

static QScriptValue color (QScriptContext * context, QScriptEngine *engine)
{
   QScriptValue s = context->argument(0);
   return engine->toScriptValue (QColor (s.toString()));
}

static QScriptValue rgb (QScriptContext * context, QScriptEngine * engine)
{
   QScriptValue r = context->argument(0);
   QScriptValue g = context->argument(1);
   QScriptValue b = context->argument(2);
   return engine->toScriptValue (QColor::fromRgb (r.toNumber (), g.toNumber(), b.toNumber()));
}

/* ---------------------------------------------------------------------- */

void JsEdit::execute (QScriptEngine & engine)
{
   QScriptValue infoObject = engine.newQObject (info);
   engine.globalObject().setProperty ("info", infoObject);

   QScriptValue colorFunc = engine.newFunction (color);
   engine.globalObject().setProperty ("color", colorFunc);

   QScriptValue rgbFunc = engine.newFunction (rgb);
   engine.globalObject().setProperty ("rgb", rgbFunc);

   QList < QObject * > clean;

   for (QGraphicsItem * item : scene->items())
   {
       QString name = item->toolTip ();
       QScriptValue old_property = engine.globalObject().property(name);
       if (! old_property.isValid())
       {
           QObject * obj = dynamic_cast < QObject * > (item);
           if (obj == nullptr)
           {
               QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item);
               if (shape != nullptr)
               {
                   obj = new JsShape (shape);
                   clean.append (obj);
               }
           }
           if (obj != nullptr && name != "")
           {
               QScriptValue scriptObject = engine.newQObject (obj);
               engine.globalObject().setProperty (name, scriptObject);
               info->append ("ADD " + name);
           }
       }
   }

   showItem (NULL, "globalObject on start", engine.globalObject(), 2);
   engine.evaluate (this->toPlainText ());
   showItem (NULL, "globalObject", engine.globalObject(), 2);

   #ifdef COMPLETION
      addToCompletion (engine.globalObject ()); // after execution, should be changed
   #endif

   // for (QObject * obj : clean)
   //     delete obj;
}

void JsEdit::debug ()
{
   QScriptEngine engine;

   #ifdef DEBUGGER
       QScriptEngineDebugger debugger;
       debugger.attachTo (&engine);
       QMainWindow * debugWindow = debugger.standardWindow ();
       debugWindow->resize (1024, 640);
   #endif

   execute (engine);
}

void JsEdit::run ()
{
   QScriptEngine engine;
   execute (engine);
}

/* ---------------------------------------------------------------------- */

void JsEdit::showItem (QTreeWidgetItem * above, QString name, QScriptValue value, int level)
{
   if (value.isValid ())
   {
      QTreeWidgetItem * node = new QTreeWidgetItem;
      node->setText (0, name + "=" + value.toString().replace("\n", ""));

      showBranch (node, value, level);

      if (above == NULL)
         tree->addTopLevelItem (node);
      else
         above->addChild (node);
   }
}

void JsEdit::showBranch (QTreeWidgetItem * above, QScriptValue branch, int level)
{
   if (level > 0 && branch.isValid ())
   {
      QScriptValueIterator iter (branch);
      while (iter.hasNext ())
      {
         iter.next ();
         showItem (above, iter.name (), iter.value (), level-1);
      }
   }
}

/* ---------------------------------------------------------------------- */

JsEdit::JsEdit (QWidget * parent, QTreeWidget * tree_param, QTextEdit * info_param, QGraphicsScene * scene_param) :
   QTextEdit (parent),
   completer (nullptr),
   tree (tree_param),
   info (info_param),
   scene (scene_param)
{
   // info->append ("init");

   JsHighlighter * highlighter = new JsHighlighter (document ());

   #ifdef COMPLETION
   setCompletion (QStringList () << "bool" << "char" << "int");
   #endif

   append ("// comment");
   append ("/* comment */");
   append ("");
   append ("info.append (\"hello\");");
   append ("");
   append ("info.setTextColor (color (\"yellow\"));");
   append ("info.append (\"yellow\");");
   append ("");
   append ("info.setTextColor (color (\"orange\"));");
   append ("info.append (\"orange\");");
   append ("");
   append ("info.setTextColor (rgb (0, 0, 255));");
   append ("info.append (\"blue\");");
   append ("");
   append ("info.setTextColor (color (\"green\"));");
   append ("");
   append ("function print (k)");
   append ("{");
   append ("   info.append (\"Hello \" + k);");
   append ("}");
   append ("");
   #ifdef DEBUGGER
   append ("debugger;");
   append ("");
   #endif
   append ("for ( i = 1; i <= 3; i++)");
   append ("   print (i);");

   append ("");
   append ("result = 7;");
   append ("");
   append ("obdelnik.setWidth (40);");
   append ("obdelnik.height = 40;");
   append ("obdelnik.brush = color ('blue');");
   append ("kruh1.brush = color ('yellow');");
   append ("kruh2.brush = color ('orange');");
}

/* ---------------------------------------------------------------------- */
