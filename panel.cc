#include "panel.h"
#include <math.h>

#include <QFileInfo>
#include <QDialogButtonBox>

#ifdef SQL
    #include <QSqlDatabase>
    #include <QSqlQuery>
    #include <QSqlRecord>
    #include <QSqlTableModel>
    #include <QSqlError>
#endif

#ifdef DBUS
   #include <QDBusConnection>
   #include <QDBusInterface>
#endif

#ifdef SVG_PANEL
    #include <QSvgWidget>
    #include <QNetworkReply>
#endif

#ifdef WEBENGINE
    #include <QWebEngineView>
#endif

#ifdef WEBKIT
    #include <QWebView>
#endif

#ifdef CHART
    #include <QtCharts/QChartView>
    #include <QtCharts/QBarSeries>
    #include <QtCharts/QBarSet>
    #include <QtCharts/QLegend>
    #include <QtCharts/QBarCategoryAxis>
    #include <QtCharts/QValueAxis>
    #include <QtCharts/QPieSeries>
    #include <QtCharts/QPieSlice>
    // QT_CHARTS_USE_NAMESPACE
#endif

#ifdef VISUALIZATION
   #include <QtDataVisualization>
   // using namespace QtDataVisualization;
#endif

#ifdef SOCKET
   #include <QTcpServer>
   #include <QTcpSocket>
#endif

#ifdef QT3D
    #include <Qt3DCore/QEntity>
    #include <Qt3DCore/QTransform>
    // #include <Qt3DCore/QAspectEngine>

    #include <Qt3DRender/qrenderaspect.h>
    #include <Qt3DRender/QCamera>
    #include <Qt3DRender/QMaterial>

    #include <Qt3DExtras/QTorusMesh>
    #include <Qt3DExtras/QCuboidMesh>
    #include <Qt3DExtras/QSphereMesh>
    #include <Qt3DExtras/QConeMesh>
    #include <Qt3DExtras/QCylinderMesh>

    #include <Qt3DExtras/QPhongMaterial>

    #include <Qt3DExtras/QOrbitCameraController>
    #include <Qt3DExtras/QFirstPersonCameraController>
    #include <Qt3DExtras/Qt3DWindow>
#endif

#include <iostream>
using namespace std;

#include "draw.h"

/* ---------------------------------------------------------------------- */

Panel::Panel ()
{
    setWindowFlags (Qt::Dialog |
                    Qt::CustomizeWindowHint |
                    Qt::WindowSystemMenuHint |
                    Qt::WindowTitleHint |
                    Qt::WindowMinMaxButtonsHint |
                    Qt::WindowCloseButtonHint);

    setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);
}

QVariant Panel::getAttr (QString name)
{
    return property (name.toLatin1());
}

void Panel::setAttr(QString name, QVariant value)
{
    setProperty (name.toLatin1(), value);
}

void Panel::setupTreeItem (MyTreeItem * node)
{
    node->setText (0, toolTip() + " : " + typeName ());
}

void Panel::displayProperties (Draw * win)
{
    for (QString name : attrNames ())
    {
        QVariant value = getAttr (name);
        win->displayLine (name, value);
    }

}

void Panel::storeProperty (QString name, QVariant value)
{
    setAttr (name, value);
}

void Panel::contextMenu (QMenu * menu)
{
   menu->addSeparator ();
   const QMetaObject * cls = this->metaObject ();
   int cnt = cls->methodCount ();
   for (int i = cls->methodOffset()-1; i < cnt; i++)
   {
       QMetaMethod m = cls->method (i);
       if (m.parameterCount() == 0 && m.methodType() == QMetaMethod::Method)
       {
           menu->addAction (m.methodSignature(), [=] () { m.invoke (this); });
       }
   }
}

void Panel::readItem(QJsonObject &obj)
{
    for (QString name : attrNames ())
    {
        if (obj.contains (name))
        {
           QVariant value = obj [name].toVariant ();
           setAttr (name, value);
        }
    }
}

void Panel::writeItem (QJsonObject & obj)
{
    for (QString name : attrNames ())
    {
        QVariant value = getAttr (name);
        obj [name] = QJsonValue::fromVariant (value);
    }
}

void Panel::readXmlItem(QXmlStreamReader &reader)
{
    QXmlStreamAttributes attr = reader.attributes();

    for (QString name : attrNames ())
    {
        if (attr.hasAttribute (name))
        {
            QString text = attr.value(name).toString();
            setAttr (name, text);
        }
    }
}

void Panel::writeXmlItem(QXmlStreamWriter & writer)
{
    for (QString name : attrNames ())
    {
        QVariant value = getAttr (name);
        writer.writeAttribute (name, value.toString ());
    }
}

/* ---------------------------------------------------------------------- */

/*
int sourceCount (QGraphicsItem * top)
{
    int cnt = 0;
    for (QGraphicsItem * item : top->childItems())
    {
        Source * src = dynamic_cast < Source * > (item);
        if (src != nullptr && src->target != nullptr)
            cnt ++;
    }
    return cnt;
}
*/

int targetCount (QGraphicsItem * top)
{
    int cnt = 0;
    for (QGraphicsItem * item : top->childItems())
    {
        Target * target = dynamic_cast < Target * > (item);
        if (target != nullptr && target->source != nullptr)
            cnt ++;
    }
    return cnt;
}

typedef QList <QGraphicsItem *> ItemList;

ItemList targetList (QGraphicsItem * start)
{
    ItemList result;
    for (QGraphicsItem * item : start->childItems())
    {
        Source * src = dynamic_cast < Source * > (item);
        if (src != nullptr && src->target != nullptr)
        {
            QGraphicsItem * stop = src->target->parentItem ();
            if (stop != nullptr)
               result << stop;
        }
    }
    return result;
}

QGraphicsLineItem * findLine (QGraphicsItem * start, QGraphicsItem * stop)
{
    QGraphicsLineItem * line = nullptr;
    for (QGraphicsItem * item : start->childItems())
    {
        Source * src = dynamic_cast < Source * > (item);
        if (src != nullptr && src->target != nullptr)
        {
            if (src->target->parentItem() == stop)
               line = src->line;
        }
    }
    return line;
}

/*
bool transferData0 (QObject * source, QObject * target)
{
    bool ok = false;
    DataPanel * source_panel = dynamic_cast < DataPanel * > (source);
    DataPanel * target_panel = dynamic_cast < DataPanel * > (target);
    if (source_panel != nullptr && target_panel != nullptr)
    {
        DataCollection data;
        data = source_panel->sendData ();
        target_panel->receiveData (data);
        ok = true;
    }
    return ok;
}
*/

bool transferData (QObject * source, QObject * target)
{
    bool ok = false;

    const QMetaObject * src_cls = source->metaObject();
    QByteArray send_signature = QMetaObject::normalizedSignature ("sendData ()");
    int send_inx = src_cls->indexOfMethod (send_signature);

    const QMetaObject * target_cls = target->metaObject();
    QByteArray receive_signature = QMetaObject::normalizedSignature ("receiveData (DataCollection)");
    int receive_inx = target_cls->indexOfMethod (receive_signature);

    if (send_inx >= 0 && receive_inx >= 0)
    {
       QMetaMethod send_func  = src_cls->method (send_inx);
       QMetaMethod receive_func  = target_cls->method (receive_inx);

       // put ("SEND");
       DataCollection value;
       ok = send_func.invoke (source, Q_RETURN_ARG (DataCollection, value));
       if (ok)
       {
           // put ("TITLE " + value.title);
           // put ("RECEIVE");
           ok = receive_func.invoke (target, Q_ARG (DataCollection, value));
           /*
           if (ok)
           {
               put ("RESULT " + value.title);
           }
           */
       }
    }
    return ok;
}

void transferItem (QGraphicsItem * item, ItemList & visited)
{
    for (QGraphicsItem * next : targetList (item))
    {
        if (! visited.contains (next))
        {
            visited << next;
            QObject * item_obj = dynamic_cast < QObject * > (item);
            QObject * next_obj = dynamic_cast < QObject * > (next);
            if (item_obj != nullptr && next_obj != nullptr)
            {
                bool ok = transferData (item_obj, next_obj);
                if (ok)
                {
                   QGraphicsLineItem * line = findLine (item, next);
                   if (line != nullptr)
                       line->setPen (QColor ("lime"));
                   transferItem (next, visited);
                }
            }
        }
    }
}

void transfer (QGraphicsScene * scene)
{
    ItemList visited;
    for (QGraphicsItem * start : scene->items())
        if (start->parentItem () == nullptr)
            if (targetCount (start) == 0)
                transferItem (start, visited);
}

void run ()
{
   transfer (getScene ());
}

void testConnection ()
{
    TablePanel * a = new TablePanel;
    TablePanel * b = new TablePanel;
    a->data.title = "table a";
    transferData (a, b);
    put ("transfer data RESULT " + b->data.title);

    DataPanel * d = new DataPanel;
    TablePanel * c = new TablePanel;
    d->data.title = "data panel";
    transferData (d, c);
    put ("transfer data RESULT " + c->data.title);

    /*
    QObject::connect (a, SIGNAL (produceData (DataCollection)), b, SLOT (receiveData (DataCollection))); // NO QUOTES
    // QObject::connect (a, &TablePanel::produceData, b, &TablePanel::receiveData);
    a->data.title = "next table";
    a->broadcastData ();
    put ("broadcast data RESULT " + b->data.title);
    */

    /*
    QObject::connect (a, SIGNAL (produceTitle (QString)), b, SLOT (receiveTitle (QString))); // NO QUOTES
    // QObject::connect (a, &TablePanel::produceTitle, b, &TablePanel::receiveTitle);
    a->data.title = "some text";
    a->broadcastTitle ();
    put ("broadcast title RESULT " + b->data.title);
    */
}

/* ---------------------------------------------------------------------- */

TextPanel::TextPanel ()
{
    edit = new QTextEdit;
    edit->setText ("some text");
    setWidget (edit);
    setWindowTitle ("Text");
}

/* ---------------------------------------------------------------------- */

DataPanel::DataPanel ()
{
    setWindowTitle ("Shape Data");
}

DataCollection DataPanel::sendData ()
{
    DataCollection data;

    data.title = "Shapes";
    // data.columns << "Name" << "Shape" << "X" << "Y" << "Width" << "Height" << "Pen" << "Brush";
    data.columns << "name" << "type" << "x" << "y" << "width" << "height" << "pen" << "brush";

    for (QGraphicsItem * item : getScene()->items ())
    // if (item->parentItem () == nullptr)
    {
         DataLine line;
         line << item->toolTip ();
         line << itemTypeName (item);
         line << item->x();
         line << item->y();

         int w = 0, h = 0;
         // QString pen, brush;
         QColor pen, brush;

         if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
         {
             // pen = penToString (shape->pen());
             // brush = brushToString (shape->brush());
             pen = shape->pen().color ();
             brush = shape->brush().color();

             if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
             {
                w = e->rect().width();
                w = e->rect().height();
             }

             if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
             {
                w  = e->rect().width();
                h = e->rect().height();
             }

            if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (shape))
            {
                 w = e->line().dx();
                 h = e->line().dy();
            }
         }

         line << w;
         line << h;
         line << pen;
         line << brush;

         data.lines.append (line);
         // put (line [0]);
    }

    return data;
}

/* ---------------------------------------------------------------------- */

TablePanel::TablePanel ()
{
    table = new QTableWidget;
    setWidget (table);

    updateTable ();
}

void TablePanel::updateTable ()
{
    setWindowTitle (data.title);

    table->clear ();
    table->setColumnCount (data.columns.length());
    table->setHorizontalHeaderLabels (data.columns);
    table->setRowCount (data.lines.length());

    int line = 0;
    for (DataLine lineData : data.lines)
    {
       int col = 0;
       for (QVariant value : lineData)
       {
           QTableWidgetItem * cell = new QTableWidgetItem;
           if (value.type() == QVariant::Color)
           {
               QColor color = value.value <QColor> ();
               cell->setData (Qt::DisplayRole, colorToString (color));
               // cell->setData (Qt::DisplayRole, value);
               if (color.red() != 0 || color.green() != 0 || color.blue() != 0)
                  cell->setData (Qt::DecorationRole, value);
           }
           else
           {
               cell->setData (Qt::DisplayRole, value);
           }
           table->setItem (line, col, cell);
           col ++;
       }
       line ++;
    }
}

/* ---------------------------------------------------------------------- */

#ifdef SQL

SqlPanel::SqlPanel ()
{
    QSplitter * vsplitter = new QSplitter ();
    vsplitter->setOrientation (Qt::Vertical);

    QSplitter * hsplitter = new QSplitter (vsplitter);

    tree = new QTreeWidget (hsplitter);
    table = new QTableView (hsplitter);
    info = new QTextEdit (vsplitter);

    setWidget (vsplitter);
    setWindowTitle ("SQL");
    resize (640, 480);

    example ();
}


void SqlPanel::message (QSqlError err)
{
    QColor save = info->textColor ();
    info->setTextColor (QColor ("red"));
    info->append ("Error:");
    info->setTextColor (QColor ("blue"));
    info->append (err.text ());
    info->setTextColor (save);
}

void SqlPanel::example ()
{
   QSqlDatabase db = QSqlDatabase::addDatabase ("QSQLITE");
   // db.setDatabaseName (":memory:");
   db.setDatabaseName ("test.sqlite");

   // dnf install qt5-qtbase-mysql
   // QSqlDatabase db = QSqlDatabase::addDatabase ("QMYSQL");

   // dnf install qt5-qtbase-postgresql
   // QSqlDatabase db = QSqlDatabase::addDatabase ("QPSQL");

   // db.setHostName ("domain.name");
   // db.setUserName ("user");
   // db.setPassword ("password");
   // db.setDatabaseName ("user");

   bool ok = db.open ();
   if (! ok)
   {
      message (db.lastError());
      return;
   }

   db.exec ("DROP TABLE IF EXISTS shapes");

   db.exec ("CREATE TABLE shapes (name TEXT, type TEXT, "
                 "x INTEGER, y INTEGER, w INTEGER, h INTEGER, "
                 "pen TEXT, brush TEXT)");

   // db.exec ("INSERT INTO colors (name, red, green, blue) VALUES (\"blue\", 0, 0, 255)");

   QSqlQuery insert (db);
   ok = insert.prepare ("INSERT INTO shapes (name, type, x, y, w, h, pen, brush) "
                                   "VALUES (:name, :type, :x, :y, :w, :h, :pen, :brush)");
   if (!ok) message (insert.lastError ());

   QGraphicsScene * scene = getScene ();
   for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
   {
       insert.bindValue (":name", item->toolTip ());
       insert.bindValue (":type", itemTypeName (item));

       int x = item->x();
       int y = item->y();
       insert.bindValue (":x", x);
       insert.bindValue (":y", y);

       int w = 0;
       int h = 0;
       QString pen = "";
       QString brush = "";

       if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
       {
          pen =  penToString (shape->pen());
          brush = brushToString (shape->brush());

           if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
           {
              QRectF r = e->rect ();
              w = r.width ();
              h = r.height();
           }

           if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
           {
               QRectF r = e->rect ();
               w = r.width ();
               h = r.height();
           }
       }

       if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
       {
          pen = penToString (e->pen());
          w = e->line().dx();
          h = e->line().dy();
       }

       insert.bindValue (":w", w);
       insert.bindValue (":h", h);
       insert.bindValue (":pen", pen);
       insert.bindValue (":brush", brush);

       ok = insert.exec ();
       if (!ok) message (insert.lastError ());
   }

   QSqlTableModel * model = new QSqlTableModel (this, db);
   model->setTable ("shapes");
   model->select ();
   table->setModel (model);

   QStringList list = db.tables();
   info->append ("tables: " + list.join (","));

   for (int i = 0; i < list.size(); i++)
   {
       QString id = list[i];
       QSqlQuery query = db.exec ("SELECT * FROM " + id);

       QTreeWidgetItem * item = new QTreeWidgetItem;
       item->setText (0, id);
       item->setForeground (0, QColor ("orange"));
       tree->addTopLevelItem (item);

       QSqlRecord rec = query.record();
       for (int k = 0; k < rec.count (); k++)
       {
          QTreeWidgetItem * subitem = new QTreeWidgetItem;
          subitem->setText (0, rec.fieldName (k));
          subitem->setForeground (0, QColor ("blue"));
          item->addChild (subitem);
       }

       tree->expandItem (item);
   }
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef DBUS

DbusPanel::DbusPanel ()
{
    info = new QTextEdit;
    setWidget (info);
    setWindowTitle ("DBus");

    QDBusConnection bus = QDBusConnection::sessionBus();
    if (bus.isConnected ())
        if (bus.registerService ("org.example.receiver"))
        {
            if (bus.registerObject ("/org/example/ReceiverObject", this, QDBusConnection::ExportAllSlots))
                info->append ("DBus Receiver ready");

            #ifdef JS
            for (QGraphicsItem * item : getScene ()->items())
            {
                QString name = item->toolTip ();
                QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item);
                if (shape != nullptr)
                {
                    QObject * obj = new JsShape (shape);
                    if (bus.registerObject ("/shape/" + name, obj, QDBusConnection::ExportAllContents))
                        info->append ("ready " + name);
                }
            }
            #endif
        }
}

DbusPanel::~DbusPanel ()
{
    // info->append ("DBus Receiver finished");
    cout << "DBus Receiver finished" << endl;
}

void DbusPanel::hello (QString s)
{
    info->append ("Receiver - method hello : " + s);
}

/*
  yum install qt-qdbusviewer
  qdbusviewer, vyhledat org.example.receiver

  nebo

  yum install qt5-qdbusviewer
  qdbusviewer-qt5
*/

#endif

/* ---------------------------------------------------------------------- */

#ifdef DBUS

DbusSendPanel::DbusSendPanel ()
{
    lineEdit = new QLineEdit;
    lineEdit->setText ("abc");

    sendButton = new QPushButton;
    sendButton->setText ("Send");
    connect (sendButton, &QPushButton::clicked, this, &DbusSendPanel::send);

    QVBoxLayout * layout = new QVBoxLayout;
    layout->addWidget (lineEdit);
    layout->addWidget (sendButton);

    QWidget * center = new QWidget;
    center->setLayout (layout);

    setWidget (center);
    setWindowTitle ("DBus Send");
    // resize (640, 480);
}

void DbusSendPanel::send ()
{
    QString text = lineEdit->text();

    QDBusInterface ifc ("org.example.receiver",
                        "/org/example/ReceiverObject"
                        /* , "org.example.ReceiverInterface" */ );

    if (ifc.isValid ())
    {
       ifc.call ("hello", text);
       put ("hello " + ifc.lastError().message());
    }

    QDBusInterface shape ("org.example.receiver",
                          "/shape/obdelnik"
                          /* , "org.example.ShapeInterface" */ );

    if (shape.isValid ())
    {
        shape.call ("setWidth", 40.0);
        put ("setWidth " + shape.lastError().message());

        shape.setProperty ("height", 40);
        put ("height " + shape.lastError().message());

        shape.setProperty ("x", 0);
        put ("x " + shape.lastError().message());

        shape.setProperty ("brushName", "lime");
        put ("brushName " + shape.lastError().message());

        shape.setProperty ("penName", "red");
        put ("penName " + shape.lastError().message());
    }
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef JS

JsWindow::JsWindow (QWidget * parent) :
   QWidget (parent)
{
    edit = new JsEdit (this, getTree (), getInfo (), getScene ());

    QPushButton * runButton = new QPushButton;
    runButton->setText ("Run");
    connect (runButton, &QPushButton::clicked, this, &JsWindow::run);

    QPushButton * debugButton = new QPushButton;
    debugButton->setText ("Debug");
    connect (debugButton, &QPushButton::clicked, this, &JsWindow::debug);

    QHBoxLayout * box = new QHBoxLayout;
    box->addStretch ();
    box->addWidget (debugButton);
    box->addWidget (runButton);

    QVBoxLayout * vlayout = new QVBoxLayout;
    vlayout->addWidget (edit);
    vlayout->addLayout (box);
    this->setLayout (vlayout);
}

void JsWindow::debug ()
{
    edit->debug ();
}

void JsWindow::run ()
{
    edit->run ();
}

JsPanel::JsPanel ()
{
    window = new JsWindow;
    setWidget (window);
    setWindowTitle ("JS");
    setToolTip ("js");
    resize (480, 320);
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef SVG_PANEL

SvgPanel::SvgPanel ()
{
    view = new QSvgWidget ();
    setWidget (view);
    setWindowTitle ("SVG");
    resize (320, 320);

    QNetworkAccessManager * manager = new QNetworkAccessManager (this);
    QObject::connect (manager, SIGNAL (finished (QNetworkReply*)),
                      this,     SLOT  (replyFinished (QNetworkReply*)) );
    // connect (manager, &QNetworkAccessManager::finished,
    //         this, &SvgPanel::replyFinished);

    QNetworkRequest request;
    request.setUrl (QUrl ("https://dev.w3.org/SVG/tools/svgweb/samples/svg-files/tiger.svg"));
    QNetworkReply * reply = manager->get (request);
 }

void SvgPanel::replyFinished (QNetworkReply * reply)
{
   QByteArray answer = reply->readAll ();
   view->load (answer);
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef SOCKET

SocketPanel::SocketPanel (int port_param) :
   port (port_param),
   server (nullptr),
   // socket (nullptr),
   info (nullptr)
{
   info = new QTextEdit;
   setWidget (info);
   setWindowTitle ("Socket");

   run ();
}

SocketPanel::~SocketPanel()
{
    // std::cerr << "SocketReciever destroyed" << std::endl;
}

void SocketPanel::print (QString s)
{
   info->append (s);
}

QByteArray readFile (QString fileName)
{
    QByteArray result;
    QFile file (fileName);
    if (file.open (QIODevice::ReadOnly))
    {
       result = file.readAll ();
       file.close ();
    }
    return result;
}

void SocketPanel::connection ()
{
    QTcpSocket * socket = server->nextPendingConnection();
    connect (socket, &QTcpSocket::disconnected, socket, &QObject::deleteLater);

    print ("New connection from " + socket->peerName() + " port " + QString::number (socket->peerPort()));

    while (!(socket->waitForReadyRead (100))) { } // <-- important, waiting for data to be read from web browser
    QByteArray request = socket->readAll();
    QList<QByteArray> lines = request.split ('\n');
    QList<QByteArray> words = lines[0].split (' ');

    QByteArray method = words[0];
    QByteArray path = words [1];
    print (method + " " + path);
    // print ("REQUEST\r\n" + request);

    QByteArray block = "";
    block += "HTTP/1.1 200 OK\r\n";

    /*
        block += "Content-Type: text/text\r\n";
        block += "\r\n";
        block += "Hello from Qt\r\n";
    */

    if (method == "GET" && path == "/")
    {
        block += "Content-Type: text/html\r\n";
        block += "\r\n";
        block += "<!DOCTYPE html>\r\n";
        block += "<html>\r\n";
        block += "   <head>\r\n";
        block += "      <title>Local</title>\r\n";
        block += "   </head>\r\n";
        block += "   <body>\r\n";
        block += "       Hello from Qt\r\n";
        block += "       <p>\r\n";
        block += "       <a href=easyui.html>easyui.html</a>\r\n";
        block += "   </body>\r\n";
        block += "</html>\r\n";
        block += "\r\n";
    }

    if (method == "GET" && path == "/draw.html")
    {
        block += "Content-Type: text/html\r\n";
        block += "\r\n";
        block += readFile (":/data/draw.html");
    }

    if (method == "GET" && path == "/easyui.html")
    {
        block += "Content-Type: text/html\r\n";
        block += "\r\n";
        block += readFile (":/data/easyui.html");
    }

    if (method == "GET" && path == "/static_table.json")
    {
        block += "Content-Type: application/json\r\n";
        block += "\r\n";
        block += readFile (":/data/table.json");
    }

    if (method == "GET" && path == "/table.json")
    {
        block += "Content-Type: application/json\r\n";
        block += "\r\n";

        QJsonArray array;
        for (DataLine line : data.lines)
        {
            QJsonObject obj;
            for (int inx = 0; inx < data.columns.count(); inx ++)
            {
               QString column = data.columns [inx];
               obj [column] = line [inx].toString ();
            }
            array.append (obj);
        }
        QJsonObject answer;
        answer ["total"] = data.lines.count();
        answer ["rows"] = array;
        QJsonDocument doc (answer);
        block += doc.toJson ();

    }

    if (method == "GET" && path == "/tree.json")
    {
        block += "Content-Type: application/json\r\n";
        block += "\r\n";
        block += readFile (":/data/tree.json");
    }

    socket->write (block);

    socket->flush ();
    socket->waitForBytesWritten (3000);

    socket->disconnectFromHost();
}

void SocketPanel::run()
{
    server = new QTcpServer (this);
    connect (server, &QTcpServer::newConnection, this, &SocketPanel::connection);
    bool ok = server->listen (QHostAddress::Any, port);
    if (ok)
       print ("Listening on port " + QString::number (port));
    else
       print ("Error when listening");
}

// http://doc.qt.io/qt-5/qtnetwork-fortuneserver-example.html

// http://www.bogotobogo.com/cplusplus/sockets_server_client.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_Client_Server.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_Multithreaded_Client_Server.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_QThreadPool_Multithreaded_Client_Server.php

// http://stackoverflow.com/questions/3122508/qt-http-server  "answer: Here is a very simple HTTP web server"

// http://github.com/qt-labs/qthttpserver/blob/master/src/httpserver/qabstracthttpserver.cpp

#endif

/* ---------------------------------------------------------------------- */

#ifdef WEBENGINE

WebEngineWindow::WebEngineWindow (QWidget * parent) :
   QWidget (parent),
   layout (nullptr),
   toolBar (nullptr),
   locationEdit (nullptr),
   view (nullptr),
   statusBar (nullptr)
{
   view = new QWebEngineView (this);
   // connect (view, SIGNAL(loadProgress(int)),  SLOT (setProgress(int)));
   // connect (view, SIGNAL(loadFinished(bool)), SLOT (adjustLocation()));
   connect (view, &QWebEngineView::loadProgress, this, &WebEngineWindow::setProgress);
   connect (view, &QWebEngineView::loadFinished, this, &WebEngineWindow::adjustLocation);

   locationEdit = new QLineEdit (this);
   locationEdit->setSizePolicy (QSizePolicy::Expanding, locationEdit->sizePolicy().verticalPolicy());
   // connect (locationEdit, SIGNAL (returnPressed()), SLOT (changeLocation()));
   connect (locationEdit, &QLineEdit::returnPressed, this, &WebEngineWindow::changeLocation);

   toolBar = new QToolBar (this);
   toolBar->addAction (view->pageAction (QWebEnginePage::Back));
   toolBar->addAction (view->pageAction (QWebEnginePage::Forward));
   toolBar->addAction (view->pageAction (QWebEnginePage::Reload));
   toolBar->addAction (view->pageAction (QWebEnginePage::Stop));
   toolBar->addWidget (locationEdit);

   statusBar = new QStatusBar (this);

   layout = new QVBoxLayout (this);
   layout->addWidget (toolBar);
   layout->addWidget (view);
   layout->addWidget (statusBar);
   layout->setStretch (0, 0);
   layout->setStretch (1, 1);
   layout->setStretch (2, 0);

   this->setLayout (layout);

   // load (QUrl ("http://doc.qt.io/qt-4.8"));
   // load (QUrl::fromLocalFile (QFileInfo ("draw.html").absoluteFilePath ()));
   // load (QUrl ("http://localhost:1234"));
   // load (QUrl ("http://localhost:1234/draw.html"));
   load (QUrl ("http://localhost:1234/easyui.html"));
}

void WebEngineWindow::load (QUrl url)
{
    view->load (url);
}

void WebEngineWindow::adjustLocation ()
{
    locationEdit->setText (view->url().toString());
}

void WebEngineWindow::changeLocation ()
{
    QString text = locationEdit->text();
    if (! text.startsWith ("file:") &&
        ! text.startsWith ("ftp:") &&
        ! text.startsWith ("http:") &&
        ! text.startsWith ("https:"))
    {
        text = "http:" + text;
    }
    QUrl url = QUrl (text);
    view->load (url);
    view->setFocus ();
}

void WebEngineWindow::setProgress (int p)
{
    statusBar->showMessage (QString::number (p) + "%");
}

WebPanel::WebPanel ()
{
   window = new WebEngineWindow ();
   setWidget (window);
   setWindowTitle ("Web Engine");
   resize (640, 480);
}

void WebPanel::load (QUrl url)
{
   window->load (url);
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef WEBKIT

WebKitWindow::WebKitWindow (QWidget * parent) :
   QWidget (parent),
   layout (nullptr),
   toolBar (nullptr),
   locationEdit (nullptr),
   view (nullptr),
   statusBar (nullptr)
{
   view = new QWebView (this);
   // connect (view, SIGNAL(loadProgress(int)),  SLOT (setProgress(int)));
   // connect (view, SIGNAL(loadFinished(bool)), SLOT (adjustLocation()));
   connect (view, &QWebView::loadProgress, this, &WebKitWindow::setProgress);
   connect (view, &QWebView::loadFinished, this, &WebKitWindow::adjustLocation);

   locationEdit = new QLineEdit (this);
   locationEdit->setSizePolicy (QSizePolicy::Expanding, locationEdit->sizePolicy().verticalPolicy());
   // connect (locationEdit, SIGNAL (returnPressed()), SLOT (changeLocation()));
   connect (locationEdit, &QLineEdit::returnPressed, this, &WebKitWindow::changeLocation);

   toolBar = new QToolBar (this);
   toolBar->addAction (view->pageAction (QWebPage::Back));
   toolBar->addAction (view->pageAction (QWebPage::Forward));
   toolBar->addAction (view->pageAction (QWebPage::Reload));
   toolBar->addAction (view->pageAction (QWebPage::Stop));
   toolBar->addWidget (locationEdit);

   statusBar = new QStatusBar (this);

   layout = new QVBoxLayout (this);
   layout->addWidget (toolBar);
   layout->addWidget (view);
   layout->addWidget (statusBar);
   layout->setStretch (0, 0);
   layout->setStretch (1, 1);
   layout->setStretch (2, 0);

   this->setLayout (layout);

   // load (QUrl ("http://doc.qt.io/qt-4.8"));
   // load (QUrl::fromLocalFile (QFileInfo ("draw.html").absoluteFilePath ()));
   // load (QUrl ("http://localhost:1234"));
   // load (QUrl ("http://localhost:1234/draw.html"));
   load (QUrl ("http://localhost:1234/easyui.html"));
}

void WebKitWindow::load (QUrl url)
{
    view->load (url);
}

void WebKitWindow::adjustLocation ()
{
    locationEdit->setText (view->url().toString());
}

void WebKitWindow::changeLocation ()
{
    QString text = locationEdit->text();
    if (! text.startsWith ("file:") &&
        ! text.startsWith ("ftp:") &&
        ! text.startsWith ("http:") &&
        ! text.startsWith ("https:"))
    {
        text = "http:" + text;
    }
    QUrl url = QUrl (text);
    view->load (url);
    view->setFocus ();
}

void WebKitWindow::setProgress (int p)
{
    statusBar->showMessage (QString::number (p) + "%");
}

WebKitPanel::WebKitPanel ()
{
   window = new WebKitWindow ();
   setWidget (window);
   setWindowTitle ("Web Kit");
   resize (640, 480);
}

void WebKitPanel::load (QUrl url)
{
   window->load (url);
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef CHART

ChartPanel::ChartPanel ()
{
    QT_CHARTS_USE_NAMESPACE

    QStringList categories;
    QBarSet * xset = new QBarSet ("X");
    QBarSet * yset = new QBarSet ("Y");

    categories << "a" << "b" << "c";
    * xset << 10 << 20 << 30;
    * yset << 100 << 200 << 100;

    QBarSeries * series = new QBarSeries ();
    series->append (xset);
    series->append (yset);

    QChart * chart = new QChart ();
    chart->addSeries (series);
    chart->setTitle ("Simple chart example");

    QBarCategoryAxis * axisX = new QBarCategoryAxis ();
    axisX->append (categories);
    chart->addAxis (axisX, Qt::AlignBottom);
    series->attachAxis (axisX);

    QValueAxis * axisY = new QValueAxis ();
    axisY->setRange (0, 200);
    chart->addAxis (axisY, Qt::AlignLeft);
    series->attachAxis (axisY);

    chart->legend()->setVisible (true);
    chart->legend()->setAlignment (Qt::AlignBottom);

    QChartView * view = new QChartView (chart);
    setWidget (view);
    setWindowTitle ("Chart");
    resize (480, 320);
}

void ChartPanel::receiveData (DataCollection data)
{
    QT_CHARTS_USE_NAMESPACE

    QStringList categories;
    QBarSeries * series = new QBarSeries ();

    for (int inx = 0; inx < data.columns.size(); inx ++)
    {
        QString column = data.columns [inx];
        categories << column;

        QBarSet * set = new QBarSet (column);

        for (DataLine line : data.lines)
        {
           *set << line [inx].toDouble();
        }

        series->append (set);
    }

    QChart * chart = new QChart ();
    chart->addSeries (series);
    chart->setTitle (data.title);

    QBarCategoryAxis * axisX = new QBarCategoryAxis ();
    axisX->append (categories);
    chart->addAxis (axisX, Qt::AlignBottom);
    series->attachAxis (axisX);

    QValueAxis * axisY = new QValueAxis ();
    axisY->setRange (0, 200);
    chart->addAxis (axisY, Qt::AlignLeft);
    series->attachAxis (axisY);

    chart->legend()->setVisible (true);
    chart->legend()->setAlignment (Qt::AlignBottom);

    QChartView * view = new QChartView (chart);
    setWidget (view);
    setWindowTitle (data.title);

    resize (480, 320);
}

PiePanel::PiePanel ()
{
    QT_CHARTS_USE_NAMESPACE
    QPieSeries * series = new QPieSeries();
    series->append ("A", 15);
    series->append ("B", 20);
    series->append ("C", 25);

    QChart * chart = new QChart();
    chart->addSeries (series);
    chart->legend()->hide();

    QChartView * view = new QChartView (chart);
    setWidget (view);
    setWindowTitle ("Pie Chart");
    resize (320, 320);
}

void PiePanel::receiveData (DataCollection data)
{
    QT_CHARTS_USE_NAMESPACE

    QPieSeries * series = new QPieSeries();

    // int name_inx = data.columns.indexOf ("Name");
    // int value_inx = data.columns.indexOf ("X");
    int name_inx = data.columns.indexOf ("name");
    int value_inx = data.columns.indexOf ("x");
    for (DataLine line : data.lines)
    {
       series->append (line [name_inx].toString(), line [value_inx].toDouble());
    }

    QChart * chart = new QChart();
    chart->addSeries (series);
    // chart->legend()->hide();

    QChartView * view = new QChartView (chart);
    setWidget (view);
    setWindowTitle (data.title);
    resize (320, 320);
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef VISUALIZATION

QWidget * createVisualizationWindow (QWidget * parent)
{
    using namespace QtDataVisualization;

    Q3DBars * bars = new Q3DBars;
    bars->rowAxis()->setRange (0, 4);
    bars->columnAxis()->setRange (0, 4);
    QBar3DSeries * series = new QBar3DSeries;
    QBarDataRow * data = new QBarDataRow;
    * data << 1.0f << 3.0f << 7.5f << 5.0f << 2.2f;
    series->dataProxy()->addRow(data);
    bars->addSeries(series);

    return QWidget::createWindowContainer (bars, parent);
}

VisualizationPanel::VisualizationPanel ()
{
    window = createVisualizationWindow (nullptr);
    setWidget (window);
    setWindowTitle ("Data Visualization");
}

#endif

/* ---------------------------------------------------------------------- */

#ifdef QT3D

// https://vicrucann.github.io/tutorials/qt3d-cmake/

void addMark (Qt3DCore::QEntity * root,  qreal x, qreal y, qreal z, QColor color, qreal r = 5)
{
    using namespace Qt3DCore;
    using namespace Qt3DExtras;
    using Qt3DCore::QTransform;

    QEntity * torus = new QEntity (root);

    QTorusMesh * mesh = new QTorusMesh;
    mesh->setRadius (r);
    mesh->setMinorRadius (r/5);

    QTransform * transform = new QTransform;
    transform->setTranslation (QVector3D (x, y, z));

    QPhongMaterial * material = new QPhongMaterial (torus);
    material->setAmbient (color);

    torus->addComponent (mesh);
    torus->addComponent (transform);
    torus->addComponent (material);
}

const qreal step = -40;

void addShape (Qt3DCore::QEntity * root,  QGraphicsItem * item, qreal x0, qreal y0, qreal z)
{
    using namespace Qt3DCore;
    using namespace Qt3DRender;
    using namespace Qt3DExtras;
    using Qt3DCore::QTransform;


    qreal x = x0 + item->x();
    qreal y = y0 + item->y();

    QString pen = "";
    QString brush = "";

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        QColor pen =  shape->pen().color();
        QColor brush = shape->brush().color();

        if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
        {
            QRectF r = e->rect ();
            qreal w = r.width ();
            qreal h = r.height();

            QEntity * entity = new QEntity (root);

            QCuboidMesh * mesh = new QCuboidMesh ();
            mesh->setXExtent (w);
            mesh->setYExtent (h);
            mesh->setZExtent (5);

            QTransform * transform = new QTransform;
            transform->setTranslation (QVector3D (x+w/2, y+h/2, z));

            QPhongMaterial * material = new QPhongMaterial (entity);
            material->setAmbient (brush);

            entity->addComponent (mesh);
            entity->addComponent (transform);
            entity->addComponent (material);

            /*
            addMark (root, x,   y,   z, "red");
            addMark (root, x,   y+h, z, "red");
            addMark (root, x+w, y,   z, "red");
            addMark (root, x+w, y+h, z, "red");
            */
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
            QRectF r = e->rect ();
            qreal w = r.width ();
            qreal h = r.height();

            QEntity * entity = new QEntity (root);

            QSphereMesh * mesh = new QSphereMesh ();
            mesh->setRadius (w/2);

            QTransform * transform = new QTransform;
            transform->setTranslation (QVector3D (x+w/2, y+w/2, z));

            QPhongMaterial * material = new QPhongMaterial (entity);
            material->setAmbient (brush);

            entity->addComponent (mesh);
            entity->addComponent (transform);
            entity->addComponent (material);

            // addMark (root, x+w/2, y+w/2, step, "red", w/2);
        }
    }

    if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
    {
        QColor color = e->pen().color ();
        qreal w = e->line().dx();
        qreal h = e->line().dy();

        qreal length = sqrt (w*w + h*h);

        qreal angle = 0;
        if (length != 0)
            angle = asin (w/length) / 3.14159 * 180.0;
        if (h < 0)
           angle = 180-angle;

        QEntity * entity = new QEntity (root);

        QCylinderMesh * mesh = new QCylinderMesh ();
        mesh->setRadius (2);
        mesh->setLength (length);


        QVector3D point (x, y, z-step);
        QVector3D axis (0, 0, 1);
        QMatrix4x4 m;
        m.translate (0, length/2, 0);
        m.translate (point);
        QMatrix4x4 n = QTransform::rotateAround (point, -angle, axis);
        m = n * m;

        QTransform * transform = new QTransform;
        transform->setMatrix(m);
        // transform->setRotation (QQuaternion::fromAxisAndAngle (QVector3D (0,0,1), -angle));
        // transform->setTranslation (QVector3D (x+length/2, y, z-step));

        QPhongMaterial * material = new QPhongMaterial (entity);
        material->setAmbient (color);

        entity->addComponent (mesh);
        entity->addComponent (transform);
        entity->addComponent (material);
    }

    for (QGraphicsItem * t : item->childItems())
        addShape (root, t, x, y, z+step);
}

QWidget * createQt3DWindow (QWidget * parent)
{
    using namespace Qt3DCore;
    using namespace Qt3DRender;
    using namespace Qt3DExtras;
    using Qt3DCore::QTransform;

    QEntity * root = new QEntity;

    QTransform * globalTransform = new QTransform;
    globalTransform->setRotation (QTransform::fromAxisAndAngle (QVector3D (1,0,0), 140.0f));
    globalTransform->setScale (0.1);
    root->addComponent (globalTransform);

    if (0)
    {
        int x = 40;
        int y = 100;
        int w = 200;
        int h = 100;
        addMark (root, x,     y, 0, "blue");
        addMark (root, x+w,   y, 0, "red");
        addMark (root, x,   y+h, 0, "green");
        addMark (root, x+w, y+h, 0, "orange");
    }

    if (0)
    {
        QEntity * torus = new QEntity (root);

        QTorusMesh * mesh = new QTorusMesh;
        mesh->setRadius (50);
        mesh->setMinorRadius (10);
        mesh->setRings (100);
        mesh->setSlices (20);

        QTransform * transform = new QTransform;
        transform->setRotation (QQuaternion::fromAxisAndAngle (QVector3D(1,0,0), 45.f ));

        QPhongMaterial * material = new QPhongMaterial (torus);
        material->setAmbient (QColor ("blue"));

        torus->addComponent (mesh);
        torus->addComponent (transform);
        torus->addComponent (material);
    }


    QGraphicsScene * scene = getScene ();
    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
        if (item->parentItem() == nullptr)
            addShape (root, item, 0, 0, 0);


    // window
    Qt3DWindow * view = new Qt3DWindow;
    view->setRootEntity (root);

    // camera
    QCamera * camera = view->camera ();
    camera->lens()->setPerspectiveProjection (45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    camera->setPosition (QVector3D (0, 0, 40.0f));
    camera->setViewCenter (QVector3D (0, 0, 0));

    // manipulator
    QOrbitCameraController* manipulator = new QOrbitCameraController (root);
    manipulator->setLinearSpeed (50.f);
    manipulator->setLookSpeed (180.f);
    manipulator->setCamera (camera);
    /*
    QFirstPersonCameraController * manipulator = new QFirstPersonCameraController (root);
    manipulator->setLinearSpeed (50.f);
    manipulator->setLookSpeed (180.f);
    manipulator->setCamera (camera);
    */

    return QWidget::createWindowContainer (view, parent);

    // https://doc.qt.io/qt-5/qwidget.html#createWindowContainer
    // The window container does not interoperate with QGraphicsProxyWidget
}

Qt3DPanel::Qt3DPanel ()
{
    window = createQt3DWindow (nullptr);
    setWidget (window);
    setWindowTitle ("Qt3D");
}

#endif

/* ---------------------------------------------------------------------- */

QStringList componentTools ()
{
    QStringList result;
    result << "text";
    result << "data";
    result << "table";
    #ifdef SQL
       result << "sql";
    #endif
    #ifdef DBUS
       result << "dbus";
       result << "dbus_send";
    #endif
    #ifdef JS
       result << "js";
    #endif
    #ifdef SVG_PANEL
       result << "svg";
    #endif
    #ifdef SOCKET
       result << "socket";
    #endif
    #ifdef WEBENGINE
       result << "web";
    #endif
    #ifdef WEBKIT
       result << "webkit";
    #endif
    #ifdef CHART
       result << "chart";
       result << "pie";
    #endif
    #ifdef VISUALIZATION
       // result << "visualization";
    #endif
    #ifdef QT3D
       // result << "qt3d";
    #endif
    return result;
}

QGraphicsItem * createComponent (QString tool)
{
    QGraphicsItem * result = nullptr;
    if (tool == "text")
    {
       TextPanel * t = new TextPanel;
       result = t;
    }
    if (tool == "data")
    {
       result = new DataPanel;
    }
    else if (tool == "table")
    {
       result = new TablePanel;
    }
    #ifdef SQL
    else if (tool == "sql")
    {
       result = new SqlPanel;
    }
    #endif
    #ifdef DBUS
    else if (tool == "dbus")
    {
       result = new DbusPanel;
    }
    else if (tool == "dbus_send")
    {
       result = new DbusSendPanel;
    }
    #endif
    #ifdef JS
    else if (tool == "js")
    {
       result = new JsPanel;
    }
    #endif
    #ifdef SVG_PANEL
    else if (tool == "svg")
    {
       result = new SvgPanel;
    }
    #endif
    #ifdef SOCKET
    else if (tool == "socket")
    {
       result = new SocketPanel;
    }
    #endif
    #ifdef WEBENGINE
    else if (tool == "web")
    {
       result = new WebPanel;
    }
    #endif
    #ifdef WEBKIT
    else if (tool == "webkit")
    {
       result = new WebKitPanel;
    }
    #endif
    #ifdef CHART
    else if (tool == "chart")
    {
       result = new ChartPanel;
    }
    else if (tool == "pie")
    {
       result = new PiePanel;
    }
    #endif
    #ifdef VISUALIZATION
    else if (tool == "visualization")
    {
       result = new VisualizationPanel;
    }
    #endif
    #ifdef QT3D
    else if (tool == "qt3d")
    {
       result = new Qt3DPanel;
    }
    #endif
    return result;
}
