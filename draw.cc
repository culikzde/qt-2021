#include "draw.h"
#include "ui_draw.h"

#include <QApplication>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

#include <QPen>
#include <QBrush>

#include <QDrag>
#include <QMimeData>
#include <QClipboard>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "colorbutton.h"
#include "toolbutton.h"
#include "panel.h"

void initColorMap ();

/* ---------------------------------------------------------------------- */

Draw * global_win = nullptr;

Draw * getWin ()
{
   return global_win;
}

MyScene * getScene ()
{
   return global_win->scene;
}

QTreeWidget * getTree ()
{
   return global_win->ui->tree;
}

QTextEdit * getInfo ()
{
   return global_win->ui->info;
}


void put (QString s)
{
    if (global_win != nullptr)
       global_win->put (s);
}

/* ---------------------------------------------------------------------- */

Draw::Draw (QWidget *parent) :
    QMainWindow (parent),
    ui (new Ui::Draw)
{
    ui->setupUi(this);
    global_win = this;
    initColorMap ();

    ui->vsplitter->setStretchFactor (0, 3);
    ui->vsplitter->setStretchFactor (1, 1);

    ui->hsplitter->setStretchFactor (0, 1);
    ui->hsplitter->setStretchFactor (1, 2);
    ui->hsplitter->setStretchFactor (2, 1);

    QToolBar * toolBar = new QToolBar (ui->toolTab);
    addToolButtons (toolBar);

    // ui->palette->setTabText (1, "Colors");
    // ui->palette->setCurrentIndex (1);
    QToolBar * colorBar = new QToolBar (ui->colorTab);
    addColorButtons (colorBar);

    scene = new MyScene (this, this);
    connect (scene, &MyScene::selectionChanged, this, &Draw::selectionChanged);
    ui->graphicsView->setScene (scene);
    ui->graphicsView->setSceneRect (0, 0, 800, 640);

    QGraphicsRectItem * r = new QGraphicsRectItem;
    r->setToolTip ("obdelnik");
    r->setRect (0, 0, 200, 100);
    r->setPen (QPen (QColor ("blue")));
    r->setBrush (QBrush (QColor ("yellow")));
    r->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);
    r->setPos (40, 100);
    scene->addItem (r);

    for (int i = 1; i <= 2; i++)
    {
        QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
        e->setToolTip ("kruh" + QString::number (i));
        e->setRect (0, 0, 40, 40);
        e->setPen (QColor ("blue"));
        e->setBrush (QColor ("cornflowerblue"));
        e->setPos (100 + 40*(2*i-3)-20, 50-20);
        e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);
        e->setParentItem (r);
    }

    #ifndef JS
       ui->menuJavaScriptView->setEnabled (false);
    #endif

    #ifndef WEBENGINE
       ui->menuWebEngineView->setEnabled (false);
    #endif

    #ifndef WEBKIT
       ui->menuWebKitView->setEnabled (false);
    #endif

    #ifndef VISUALIZATION
       ui->menuVisualizationView->setEnabled (false);
    #endif

    #ifndef QT3D
       ui->menuQt3DView->setEnabled (false);
    #endif

    displayTree ();
    ui->statusbar->showMessage (QString ("Qt ") + qVersion ());
}

Draw::~Draw()
{
    delete ui;
}

void Draw::put (QString s)
{
    ui->info->append (s);
}

/* ---------------------------------------------------------------------- */

const int markSize = 8;

Source::Source () :
    rel (0),
    line (nullptr),
    target (nullptr)
{
    setRect (0, 0, markSize, markSize);
    setBrush (QColor ("lime"));
    setPen (QColor ("orange"));
    setFlags (QGraphicsItem::ItemIsMovable |
              QGraphicsItem::ItemIsSelectable |
              QGraphicsItem::ItemSendsScenePositionChanges );
}

void Source::updateLine ()
{
    if (line != nullptr && target != nullptr)
    {
        QPointF point (markSize/2, markSize/2);
        point = mapFromItem (target, point);
        line->setPos (markSize/2, markSize/2);
        line->setLine (0, 0, point.x()-markSize/2, point.y()-markSize/2);
    }
}

QVariant Source::itemChange (QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemScenePositionHasChanged)
    {
        updateLine ();
    }
    return QGraphicsItem::itemChange(change, value);
}

/* ---------------------------------------------------------------------- */

Target::Target () :
    rel (0),
    source (nullptr)
{
    setRect (0, 0, markSize, markSize);
    setBrush (QColor ("yellow"));
    setPen (QColor ("orange"));
    setFlags (QGraphicsItem::ItemIsMovable |
              QGraphicsItem::ItemIsSelectable |
              QGraphicsItem::ItemSendsScenePositionChanges );
}

QVariant Target::itemChange (QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemScenePositionHasChanged)
    {
        if (source != nullptr)
            source->updateLine ();
    }
    return QGraphicsItem::itemChange(change, value);
}

/* ---------------------------------------------------------------------- */

void MyScene::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mimeData = event->mimeData ();
    if (mimeData->hasColor () || mimeData->hasFormat (toolFormat))
       event->setAccepted (true);
    else
       event->setAccepted (false);
}

void MyScene::dragMoveEvent (QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * mimeData = event->mimeData ();
    if (mimeData->hasColor () || mimeData->hasFormat (toolFormat))
       event->setAccepted (true);
    else
       event->setAccepted (false);
}

void MyScene::dropEvent (QGraphicsSceneDragDropEvent * event)
{
   const QMimeData * mimeData = event->mimeData ();
   if (mimeData->hasColor ())
   {
       QColor c = mimeData->colorData().value < QColor > ();

       QGraphicsItem * item = itemAt (event->scenePos(), QTransform ());
       QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item);
       if (shape != nullptr)
       {
          if (event->proposedAction() == Qt::CopyAction) /* ctrl mouse */
              shape->setPen (c);
          else
              shape->setBrush (c);
       }
       else
       {
          Panel * panel = dynamic_cast <Panel * > (item);
          if (panel != nullptr)
          {
          }
          else
          {
             setBackgroundBrush (c);
          }
       }
   }
   else if (mimeData->hasFormat (toolFormat))
   {
       QString tool = mimeData->data (toolFormat);
       QPointF point = event->scenePos ();
       QGraphicsItem * location = itemAt (point, QTransform ());

       QGraphicsItem * e = nullptr;

       if (tool == "rectangle")
       {
          QGraphicsRectItem * r = new QGraphicsRectItem;
          r->setRect (0, 0, 100, 50);
          r->setBrush (QColor ("lime"));
          r->setPen (QColor ("orange"));
          e = r;
          e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);
       }
       else if (tool == "ellipse")
       {
          QGraphicsEllipseItem * t = new QGraphicsEllipseItem;
          t->setRect (0, 0, 100, 50);
          t->setBrush (QColor ("cornflowerblue"));
          t->setPen (QColor ("yellow"));
          e = t;
          e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);
       }
       else if (tool == "line")
       {
          Source * source;
          Target * target;
          createSourceAndTarget (source, target);

          target->setParentItem (source);
          target->setPos (100, 100);

          source->updateLine ();
          e = source;
       }
       else
       {
          e = createComponent (tool);
       }

       if (e != nullptr)
       {
           place (e, location, point);
       }

       win->displayTree ();
   }
}

void MyScene::createSourceAndTarget (Source * & source, Target * & target)
{
    source = new Source ();
    target = new Target ();

    QGraphicsLineItem * line = new QGraphicsLineItem;
    line->setPen (QColor ("red"));
    line->setParentItem (source);

    source->line = line;
    source->target = target;
    target->source = source;

    relCnt ++;
    source->rel = relCnt;
    target->rel = relCnt;

    source->setToolTip ("source");
    target->setToolTip ("target");
    line->setToolTip ("line");
}

void MyScene::place (QGraphicsItem * item, QGraphicsItem * location, QPointF point)
{
    if (location != nullptr)
    {
        point = location->mapFromScene (point);
        item->setPos (point);
        item->setParentItem (location);
    }
    else
    {
        item->setPos (point);
        this->addItem (item);
    }
}

void MyScene::placeAtPoint (QGraphicsItem * item, QPointF point)
{
    QGraphicsItem * location = itemAt (point, QTransform ());
    place (item, location, point);
}

/* ---------------------------------------------------------------------- */

void MyScene::contextMenuEvent (QGraphicsSceneContextMenuEvent * event)
{
    QPointF point = event->scenePos ();
    QGraphicsItem * location = itemAt (point, QTransform ());
    if (location != nullptr)
    {
        QMenu * menu = new QMenu ();
        menu->addAction ("move up", this, [=] { moveUp (location); });
        menu->addAction ("move down", this, [=] { moveDown (location); });
        menu->addAction ("link", this, [=] { linkItem (location, point); });
        if (Panel * panel = dynamic_cast < Panel * > (location))
        {
           panel->contextMenu (menu);
        }
        menu->exec (QCursor::pos());
    }
}

void MyScene::moveUp (QGraphicsItem * item)
{
    item->setZValue (item->zValue() + 1);
}

void MyScene::moveDown (QGraphicsItem * item)
{
    item->setZValue (item->zValue() - 1);
}

void MyScene::linkItem (QGraphicsItem * item, QPointF point)
{
    QGraphicsItem * result = nullptr;
    for (QGraphicsItem * t : items (point))
        if (t != item && result == nullptr)
            result = t;
    if (result != nullptr)
    {
        QPointF pos = QPointF (0, 0);
        pos = result->mapFromItem (item, pos);
        item->setParentItem (result);
        item->setPos (pos);
    }
}

/* ---------------------------------------------------------------------- */

void MyScene::mousePressEvent (QGraphicsSceneMouseEvent * event)
{
    Qt::MouseButton button = event->button () ;
    Qt::KeyboardModifiers modif = event->modifiers ();
    if (button == Qt::MiddleButton || button == Qt::LeftButton && modif == Qt::SHIFT)
    {
       startPos = event->scenePos();
    }
    else
    {
       QGraphicsScene::mousePressEvent (event);
    }
}

void MyScene::mouseReleaseEvent (QGraphicsSceneMouseEvent * event)
{
    Qt::MouseButton button = event->button () ;
    Qt::KeyboardModifiers modif = event->modifiers ();
    if (button == Qt::MiddleButton || button == Qt::LeftButton && modif == Qt::SHIFT)
    {
        QPointF stopPos = event->scenePos();

        Source * source;
        Target * target;
        createSourceAndTarget (source, target);
        placeAtPoint (source, startPos);
        placeAtPoint (target, stopPos);
        source->updateLine ();

        win->displayTree ();
    }
    else
    {
        QGraphicsScene::mouseReleaseEvent (event);
    }
}

void MyScene::mouseDoubleClickEvent (QGraphicsSceneMouseEvent * event)
{
    /*
    QPointF point = event->scenePos ();
    QGraphicsItem * location = itemAt (point, QTransform ());

    for (QGraphicsItem * item : selectedItems ())
        item->setSelected (false);

    if (location != nullptr)
        location->setSelected (true);
    */
}

/* ---------------------------------------------------------------------- */

void MyScene::initInput ()
{
   renameMap.clear ();
   sourceMap.clear ();
   targetMap.clear ();
   sourceList.clear ();
   targetList.clear ();
}

int MyScene::renameRel (int orig_rel)
{
    int rel = 0;
    if (orig_rel != 0)
    {
        if (renameMap.contains(orig_rel))
        {
            rel = renameMap [orig_rel];
        }
        else
        {
            relCnt ++;
            rel = relCnt;
            renameMap [orig_rel] = rel;
        }
    }
    return rel;
}

void MyScene::storeSource (Source * source)
{
    source->rel = renameRel (source->rel);
    if (source->rel != 0)
       sourceMap [source->rel] = source;
    sourceList.append (source);
}

void MyScene::storeTarget (Target * target)
{
    target->rel = renameRel (target->rel);
    if (target->rel != 0)
       targetMap [target->rel] = target;
    targetList.append (target);
}

void MyScene::completeInput ()
{
    for (Source * s : sourceList)
        if (targetMap.contains(s->rel))
            s->target = targetMap [s->rel];

    for (Target * t : targetList)
        if (sourceMap.contains(t->rel))
            t->source = sourceMap [t->rel];

    for (Source * s : sourceList)
        for (QGraphicsItem * item : s->childItems ())
            if (QGraphicsLineItem * line = dynamic_cast <QGraphicsLineItem *> (item))
            {
                s->line = line;
                s->updateLine ();
            }

    renameMap.clear ();
    sourceMap.clear ();
    targetMap.clear ();
    sourceList.clear ();
    targetList.clear ();
}

/* ---------------------------------------------------------------------- */

void Draw::displayTree ()
{
    ui->tree->clear ();
    ui->tree->header()->setVisible (false);
    treeMap.clear ();

    QTreeWidgetItem * branch = ui->tree->invisibleRootItem ();
    for (QGraphicsItem * item : scene->items ())
        if (item->parentItem() == nullptr)
           displayBranch (branch, item);

    ui->tree->expandAll();
}

inline QString str (qreal r)
{
    return QString::number (r);
}

void Draw::displayBranch (QTreeWidgetItem * branch, QGraphicsItem * item)
{
    MyTreeItem * node = new MyTreeItem;
    node->graph = item;
    treeMap [item] = node;

    QString name = item->toolTip();

    if (QGraphicsRectItem * r = dynamic_cast <QGraphicsRectItem *> (item))
    {
        name = name + " : rectangle " + str (r->rect().width()) + " x " + str (r->rect().height());
    }
    else if (auto e = dynamic_cast <QGraphicsEllipseItem *> (item))
    {
        name = name + " : ellipse " + str (e->rect().width()) + " x " + str (e->rect().height());
    }
    else if (auto t = dynamic_cast <QGraphicsLineItem *> (item))
    {
        name = name + " : line";
    }

    QAbstractGraphicsShapeItem * shape = dynamic_cast <QAbstractGraphicsShapeItem *> (item);

    node->setText (0, name);
    node->setToolTip (0, item->toolTip());
    if (shape != nullptr)
    {
        node->setData (0, Qt::DecorationRole, shape->brush().color());
    }

    if (Panel * panel = dynamic_cast < Panel * > (item))
    {
       panel->setupTreeItem (node);
    }

    for (QGraphicsItem * t : item->childItems())
        displayBranch (node, t);
    branch->addChild (node);
}

void Draw::on_tree_itemClicked (QTreeWidgetItem *item, int column)
{
    MyTreeItem * node = dynamic_cast < MyTreeItem * > (item);
    if (node != nullptr)
    {
        for (auto t : scene->selectedItems())
            t->setSelected (false);
        node->graph->setSelected (true);
        displayProperties (node->graph);
    }
}

/* ---------------------------------------------------------------------- */

void Draw::selectionChanged ()
{
    if (scene->selectedItems().count() == 1)
    {
        QGraphicsItem * item = scene->selectedItems()[0];

        for (QTreeWidgetItem * node : ui->tree->selectedItems())
            node->setSelected(false);

        if (treeMap.contains (item))
        {
           MyTreeItem * node = treeMap [item];
           node->setSelected(true);
        }

        displayProperties (item);
    }
}

/* ---------------------------------------------------------------------- */

void Draw::displayProperties (QGraphicsItem * item)
{
    ui->table->setColumnCount (2);
    ui->table->setRowCount (0);
    ui->table->setHorizontalHeaderLabels(QStringList () << "Name" << "Value");

    prop_object = nullptr; // disable storeProperty

    displayLine ("Tooltip", item->toolTip());

    displayLine ("X", item->pos().x());
    displayLine ("Y", item->pos().y());

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast <QAbstractGraphicsShapeItem *> (item))

    {
        if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
        {
            QRectF r = e->rect ();
            displayLine ("Width", r.width());
            displayLine ("Height", r.height());
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
            QRectF r = e->rect ();
            displayLine ("Width", r.width());
            displayLine ("Height", r.height());
        }

        displayLine ("Pen", shape->pen().color());
        displayLine ("Brush", shape->brush().color());

        if (Source * s = dynamic_cast < Source * > (shape))
            displayLine ("Rel", s->rel);

        if (Target * t = dynamic_cast < Target * > (shape))
            displayLine ("Rel", t->rel);
    }

    if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
    {
        QLineF r = e->line ();
        displayLine ("Width", r.x2());
        displayLine ("Height", r.y2());
        displayLine ("Pen", e->pen().color());
    }

    if (Panel * panel = dynamic_cast < Panel * > (item))
    {
        panel->displayProperties (this);
    }

    prop_object = item;
}

QTableWidgetItem * Draw::displayLine (QString name, QVariant value)
{
    int line = ui->table->rowCount ();
    ui->table->setRowCount (line+1);

    QTableWidgetItem * item = new QTableWidgetItem;
    item->setText (name);
    ui->table->setItem (line, 0, item);

    item = new QTableWidgetItem;
    item->setData (Qt::DisplayRole, value);
    item->setData (Qt::DecorationRole, value);
    ui->table->setItem (line, 1, item);

    return item;
}

/* ---------------------------------------------------------------------- */

void Draw::storeProperty (QGraphicsItem * item, QString name, QVariant value)
{
    if (name == "tooltip") item->setToolTip (value.toString ());
    if (name == "x") item->setX (value.toInt ());
    if (name == "y") item->setY (value.toInt ());

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        if (name == "pen")
        {
           QColor color = value.value <QColor> ();
           shape->setPen (color);
        }

        if (name == "brush")
        {
            QColor color = value.value <QColor> ();
            shape->setBrush (color);
        }

        if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
        {
            if (name == "width")
            {
               QRectF r = e->rect ();
               r.setWidth (value.toInt ());
               e->setRect (r);
            }
            if (name == "height")
            {
               QRectF r = e->rect ();
               r.setHeight (value.toInt ());
               e->setRect (r);
            }
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
           if (name == "width")
           {
              QRectF r = e->rect ();
              r.setWidth (value.toInt ());
              e->setRect (r);
           }
           if (name == "height")
           {
              QRectF r = e->rect ();
              r.setHeight (value.toInt ());
              e->setRect (r);
           }
        }
    }

    if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
    {
       if (name == "pen")
       {
           QColor color = value.value <QColor> ();
           e->setPen (color);
       }
       if (name == "width")
       {
          QLineF r = e->line ();
          int w = value.toInt ();
          e->setLine (0, 0, w, r.y2 ());
       }
       if (name == "height")
       {
          QLineF r = e->line ();
          int h = value.toInt ();
          e->setLine (0, 0, r.x2 (), h);
       }
    }

    if (Panel * panel = dynamic_cast < Panel * > (item))
    {
        panel->storeProperty (name, value);
    }
}

void Draw::on_table_itemChanged (QTableWidgetItem * cell)
{
    int line = cell->row ();
    int col = cell->column ();
    if (col == 1 && prop_object != nullptr)
    {
        QString name = ui->table->item (line, 0)->text ();
        QVariant value = cell->data (Qt::EditRole);
        name = name.toLower ();
        put ("SET " + name + " = " + value.toString());
        storeProperty (prop_object, name, value);
        /*
        if (name == "tooltip")
        {
            win->refreshTreeName (graphics_item, value.toString ());
        }
        if (value.type() == QVariant::Color)
        {
            cell->setData (Qt::DecorationRole, value);
        }
        */
    }
}

/* ---------------------------------------------------------------------- */

QString itemTypeName (QGraphicsItem * item)
{
    QString result = "node";

    if (dynamic_cast < Source * > (item) != nullptr)
       result = "source";
    else if (dynamic_cast < Target * > (item) != nullptr)
       result = "target";
    else if (dynamic_cast < QGraphicsRectItem * > (item) != nullptr)
       result = "rectangle";

    if (dynamic_cast < QGraphicsEllipseItem * > (item) != nullptr)
       result = "ellipse";
    if (dynamic_cast < QGraphicsLineItem * > (item) != nullptr)
       result = "line";
    if (Panel * panel = dynamic_cast < Panel * > (item))
        result = panel->typeName ();
    return result;
}

QGraphicsItem * createItem (QString type_name)
{
    QGraphicsItem * result = nullptr;

    if (type_name == "rectangle")
        result = new QGraphicsRectItem;
    else if (type_name == "ellipse")
        result = new QGraphicsEllipseItem;
    else if (type_name == "line")
        result = new QGraphicsLineItem;
    else if (type_name == "source")
        result = new Source;
    else if (type_name == "target")
        result = new Target;
    else
        result = createComponent (type_name);

    return result;
}

/* ---------------------------------------------------------------------- */

QMap <QString, QString> color_map;

void initColorMap ()
{
    for (QString name : QColor::colorNames())
    {
        QColor c (name);
        QString txt = c.name ();
        color_map [txt] = name;
    }
}

QString colorToString (QColor c)
{
    QString txt = c.name ();
    if (color_map.contains (txt))
        return color_map [txt];
    else
        return txt;
}

QString penToString (QPen p)
{
    return colorToString (p.color());
}

QString brushToString (QBrush b)
{
    return colorToString (b.color());
}

/* ---------------------------------------------------------------------- */

QColor getColor (QString name, QString default_name)
{
    if (QColor::isValidColor (name))
       return QColor (name);
    else
       return QColor (default_name);
}

void readItems (QJsonObject obj, MyScene * scene,  QGraphicsItem * target);

void readItem (QJsonObject obj, MyScene * scene,  QGraphicsItem * target)
{
    QString type = obj ["type"].toString ();
    QGraphicsItem * item = createItem (type);
    if (item != nullptr)
    {
       item->setToolTip (obj ["name"].toString ());

       int x = obj ["x"].toInt ();
       int y = obj ["y"].toInt ();
       item->setPos (x, y);

       QColor c = getColor (obj ["pen"].toString (), "red");
       QColor d = getColor (obj ["brush"].toString (), "yellow");

       if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
       {
          shape->setPen (c);
          shape->setBrush (d);

          if (QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (shape))
          {
              int w = obj ["width"].toInt (100);
              int h = obj ["height"].toInt (80);
              r->setRect (0, 0, w, h);
          }

          if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
          {
              int w = obj ["width"].toInt (100);
              int h = obj ["height"].toInt (80);
              e->setRect (0, 0, w, h);
          }

          if (Source * s = dynamic_cast < Source * > (shape))
          {
             s->rel = obj ["rel"].toInt (0);
             scene->storeSource (s);
          }

          if (Target * t = dynamic_cast < Target * > (shape))
          {
             t->rel = obj ["rel"].toInt (0);
             scene->storeTarget (t);
          }
       }

       if (QGraphicsLineItem * t = dynamic_cast < QGraphicsLineItem * > (item))
       {
           int w = obj ["width"].toInt (100);
           int h = obj ["height"].toInt (80);
           t->setLine (0, 0, w, h);
           t->setPen (c);
       }

       if (Panel * panel = dynamic_cast < Panel * > (item))
       {
          panel->readItem (obj);
       }

       item->setFlag (QGraphicsItem::ItemIsMovable);
       item->setFlag (QGraphicsItem::ItemIsSelectable);

       readItems (obj, scene, item); // read inner items, add to this item

       if (target != nullptr)
          item->setParentItem (target);
       else
          scene->addItem (item);
    }
}

void readItems (QJsonObject obj, MyScene * scene,  QGraphicsItem * target)
{
    if (obj.contains ("items") && obj ["items"].isArray())
    {
        QJsonArray list = obj ["items"].toArray ();
        for (QJsonValue item : list)
        {
            if (item.isObject ())
               readItem (item.toObject (), scene, target);
        }
    }
}

void readJson (QByteArray code, MyScene * scene,  QGraphicsItem * target)
{
    QJsonDocument doc = QJsonDocument::fromJson (code);
    QJsonObject obj = doc.object ();
    scene->initInput ();
    readItems (obj, scene, target);
    scene->completeInput ();
}

/* ---------------------------------------------------------------------- */

QJsonObject writeItem (QGraphicsItem * item)
{
    QJsonObject obj;

    obj ["type"] = itemTypeName (item);
    obj ["name"] = item->toolTip ();

    obj ["x"] = item->x();
    obj ["y"] = item->y();

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        obj ["pen"] = penToString (shape->pen());
        obj ["brush"] = brushToString (shape->brush());

        if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
        {
           obj ["width"]  = e->rect().width();
           obj ["height"] = e->rect().height();
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
           obj ["width"]  = e->rect().width();
           obj ["height"] = e->rect().height();
        }

       if (Source * s = dynamic_cast < Source * > (shape))
           obj ["rel"] = s->rel;


       if (Target * t = dynamic_cast < Target * > (shape))
           obj ["rel"] = t->rel;
    }

    if (QGraphicsLineItem * line = dynamic_cast < QGraphicsLineItem * > (item))
    {
         obj ["width"]  = line->line().dx();
         obj ["height"] = line->line().dy();
         obj ["pen"]    = penToString (line->pen());
    }

    if (Panel * panel = dynamic_cast < Panel * > (item))
    {
       panel->writeItem (obj);
    }

    QJsonArray list;
    for (QGraphicsItem * t : item->childItems ())
    {
        QJsonObject v = writeItem (t);
        list.append (v);
    }
    obj ["items"] = list;

    return obj;
}

QByteArray writeJson (QGraphicsScene * scene)
{
    QJsonObject obj;
    QJsonArray list;
    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
       if (item->parentItem() == nullptr)
          list.append (writeItem (item));
    }
    obj ["items"] = list;

    QJsonDocument doc (obj);
    QByteArray code = doc.toJson ();
    return code;
}

/* ---------------------------------------------------------------------- */

QString getString (QXmlStreamAttributes & attr, QString name)
{
    QString result = "";
    if (attr.hasAttribute (name))
    {
        result = attr.value(name).toString();
    }
    return result;
}

int getNumber (QXmlStreamAttributes & attr, QString name, int init)
{
    int result = init;
    if (attr.hasAttribute (name))
    {
        bool ok;
        result = attr.value(name).toInt (&ok);
        if (! ok)
            result = init;
    }
    return result;
}

QColor getColor (QXmlStreamAttributes & attr, QString name, QColor init)
{
    QColor result = init;
    if (attr.hasAttribute (name))
    {
        QString s = attr.value(name).toString ();
        result = QColor (s);
    }
    return result;
}

QGraphicsItem * readXmlItem (QXmlStreamReader & reader, MyScene * scene, QGraphicsItem * target)
{
    QString name = reader.name().toString();
    QGraphicsItem * item = createItem (name);

    if (item != nullptr)
    {
        QXmlStreamAttributes attr = reader.attributes();

        QString name = getString (attr, "name");
        item->setToolTip (name);

        int x = getNumber (attr, "x");
        int y = getNumber (attr, "y");
        item->setPos (x, y);

        QColor c = getColor (attr, "pen", QColor ("red"));
        QColor d = getColor (attr, "brush", QColor ("yellow"));

        int w = getNumber (attr, "width", 100);
        int h = getNumber (attr, "height", 80);

        if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
        {
           shape->setPen (c);
           shape->setBrush (d);

           if (QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (shape))
           {
               r->setRect (0, 0, w, h);
           }

           if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
           {
              e->setRect (0, 0, w, h);
           }

           if (Source * s = dynamic_cast < Source * > (shape))
           {
              s->rel = getNumber (attr, "rel", 0);
              scene->storeSource (s);
           }

           if (Target * t = dynamic_cast < Target * > (shape))
           {
              t->rel = getNumber (attr, "rel", 0);
              scene->storeTarget (t);
           }
        }

        if (QGraphicsLineItem * line = dynamic_cast < QGraphicsLineItem * > (item))
        {
            line->setLine (0, 0, w, h);
            line->setPen (c);
        }

        if (Panel * panel = dynamic_cast < Panel * > (item))
        {
           panel->readXmlItem (reader);
        }

        item->setFlag (QGraphicsItem::ItemIsMovable);
        item->setFlag (QGraphicsItem::ItemIsSelectable);

        // add item
        if (target != nullptr)
           item->setParentItem (target);
        else
           scene->addItem (item);
    }

    return item;
}

void readXmlData (QXmlStreamReader & reader, MyScene * scene, QGraphicsItem * target)
{
    scene->initInput ();
    bool top = true;
    while (! reader.atEnd())
    {
        if (reader.isStartElement())
        {
            if (reader.name().toString() == "data" && top)
            {
                top = false; // data ... top level element
            }
            else
            {
                QGraphicsItem * item = readXmlItem (reader, scene, target);
                if (item != nullptr)
                   target = item;
            }
        }
        else if (reader.isEndElement())
        {
            if (target != nullptr)
                target = target->parentItem ();
        }
        reader.readNext();
    }
    scene->completeInput ();
}

/* ---------------------------------------------------------------------- */

void writeXmlItem (QXmlStreamWriter & writer, QGraphicsItem * item)
{
    writer.writeStartElement (itemTypeName (item));

    writer.writeAttribute ("name", item->toolTip());
    writer.writeAttribute ("x", QString::number ( item->x() ));
    writer.writeAttribute ("y", QString::number ( item->y() ));

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        writer.writeAttribute ("pen", penToString (shape->pen()));
        writer.writeAttribute ("brush", brushToString (shape->brush()));

        if (QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (shape))
        {
           writer.writeAttribute ("width", QString::number ( r->rect().width() ));
           writer.writeAttribute ("height", QString::number ( r->rect().height() ));
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
           writer.writeAttribute ("width", QString::number ( e->rect().width() ));
           writer.writeAttribute ("height", QString::number ( e->rect().height() ));
        }

        if (Source * s = dynamic_cast < Source * > (shape))
            writer.writeAttribute ("rel", QString::number ( s->rel ));

        if (Target * t = dynamic_cast < Target * > (shape))
            writer.writeAttribute ("rel", QString::number ( t->rel ));
    }

    if (QGraphicsLineItem * line = dynamic_cast < QGraphicsLineItem * > (item))
    {
        writer.writeAttribute ("pen", penToString (line->pen()));
    }

    if (Panel * panel = dynamic_cast < Panel * > (item))
    {
       panel->writeXmlItem (writer);
    }

    for (QGraphicsItem * t : item->childItems())
        writeXmlItem  (writer, t);

    writer.writeEndElement ();
}

void writeXmlStart (QXmlStreamWriter & writer)
{
    writer.setAutoFormatting (true);
    writer.writeStartDocument ();
    writer.writeStartElement ("data");
}

void writeXmlEnd (QXmlStreamWriter & writer)
{
    writer.writeEndElement (); // end of data
    writer.writeEndDocument ();
}

/* ---------------------------------------------------------------------- */

void Draw::on_menuCut_triggered()
{

}

void Draw::on_menuCopy_triggered()
{
    QString code = "";
    QXmlStreamWriter writer (& code);
    writeXmlStart (writer);

    for (QGraphicsItem * item : scene->selectedItems ())
    {
        writeXmlItem (writer, item);
    }

    writeXmlEnd (writer);

    QMimeData * data = new QMimeData;
    data->setData (shapeFormat, code.toLatin1 ());
    data->setText (code.toLatin1 ());

    QClipboard * clip = QApplication::clipboard ();
    clip->setMimeData (data);
}

void Draw::on_menuPaste_triggered()
{
    QClipboard * clip = QApplication::clipboard ();
    const QMimeData * data = clip->mimeData ();
    if (data->hasFormat (shapeFormat))
    {
       QString code = data->data (shapeFormat);
       QXmlStreamReader reader (code);

       QGraphicsItem * location = nullptr;
       if (scene->selectedItems().count() == 1)
          location = scene->selectedItems()[0];

       readXmlData (reader, scene, location);
    }
    displayTree ();
}

/* ---------------------------------------------------------------------- */

void Draw::loadFile (QString fileName)
{
    QFile f (fileName);
    if (f.open (QFile::ReadOnly))
    {
        if (fileName.endsWith (".json"))
        {
           QByteArray code = f.readAll ();
           readJson (code, scene, nullptr);
        }
        else
        {
           QXmlStreamReader reader (& f);
           readXmlData (reader, scene, nullptr);
        }
    }
}

void Draw::saveFile (QString fileName)
{
    QFile f (fileName);
    if (f.open (QFile::WriteOnly))
    {
       if (fileName.endsWith (".json"))
       {
           QByteArray code = writeJson (scene);
           f.write (code);
       }
       else
       {
           QXmlStreamWriter writer (& f);
           writeXmlStart (writer);

           for (QGraphicsItem * item : scene->items ())
               if (item->parentItem () == nullptr)
                  writeXmlItem (writer, item);

           writeXmlEnd (writer);
       }
    }
    else
    {
       QMessageBox::warning (this, "Save File Error", "Cannot write file: " + fileName);
    }
}

/* ---------------------------------------------------------------------- */

const QString dir = QString ();
const QString filter = "Json files (*.json);;XML files (*.xml)";

void Draw::on_menuOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open file", dir, filter);
    if (fileName != "")
    {
        loadFile (fileName);
        displayTree ();
    }
}

void Draw::on_menuSave_triggered()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save file", dir, filter);
    if (fileName != "")
        saveFile (fileName);
}

void Draw::on_menuRun_triggered()
{
   run ();
}

void Draw::on_menuTree_triggered()
{
    displayTree ();
}

void Draw::on_menuQuit_triggered()
{
    this->close ();
}

/* ---------------------------------------------------------------------- */

void Draw::on_menuJavaScriptView_triggered()
{
   #ifdef JS
      JsWindow * widget = new JsWindow ();
      widget->show ();
   #endif
}

void Draw::on_menuWebEngineView_triggered()
{
   #ifdef WEBENGINE
      QWidget * widget = new WebEngineWindow (this);
      int inx = ui->tabs->addTab (widget, "Web Engine");
      ui->tabs->setCurrentIndex (inx);
   #endif
}

void Draw::on_menuWebKitView_triggered()
{
    #ifdef WEBKIT
       QWidget * widget = new WebKitWindow (this);
       int inx = ui->tabs->addTab (widget, "Web Kit");
       ui->tabs->setCurrentIndex (inx);
    #endif
}

void Draw::on_menuVisualizationView_triggered()
{
    #ifdef VISUALIZATION
        QWidget * widget = createVisualizationWindow (this);
        int inx = ui->tabs->addTab (widget, "Visualization");
        ui->tabs->setCurrentIndex (inx);
    #endif
}

void Draw::on_menuQt3DView_triggered()
{
    #ifdef QT3D
        QWidget * widget = createQt3DWindow (this);
        int inx = ui->tabs->addTab (widget, "Qt3D");
        ui->tabs->setCurrentIndex (inx);
    #endif
}

/* ---------------------------------------------------------------------- */

int main (int argc, char *argv[])
{
    QApplication a (argc, argv);
    Draw * w = new Draw;
    w->show();
    return a.exec();
}
